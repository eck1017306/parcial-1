//
//  main.c
//  Parcial1
//
//  Created by Erick Molina Vázquez on 17/09/15.
//  Copyright (c) 2015 Erick Molina Vázquez. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    int nomina;
    char nombre[30];
    char apellidoP[30];
    char apellidoM[30];
    int salario;
    char puesto[50];
    char fnac[50];
    char fing[50];
}ingeniero;

typedef struct
{
    int pisos;
    int simetrico;
    int diaminf;
    int diamsup;
    int techo;
    int periodo;
    ingeniero inge;
}edificacion;

int toted= 0;
int toting = 0;

//def metodos

edificacion* addedificacion(edificacion* ed, ingeniero* inge);
ingeniero* addinge(ingeniero* inge);
void imprimeingeniero(edificacion* ed);
void imprimerh(edificacion* ed, ingeniero* inge);
void menuinge(edificacion* ed, ingeniero* inge);
void menurh(edificacion* ed, ingeniero* inge);

//**MAIN**
int main(int argc, const char * argv[]) {
    int op = 0;
    
    edificacion *ed = (edificacion *)malloc(sizeof(edificacion));
    ingeniero *inge = (ingeniero *)malloc(sizeof(ingeniero)); // variable donde se dara acceso a los trabajadores
    
    char * menu = (char *)malloc(sizeof(char)*300);
    strcpy(menu,"\n::::\tMenu Principal\t::::\n1. Ingenieros\n2. Recursos humanos\n3. Salir\n\nFavor de ingresar una opción.\n");
    while(op!=3)
    {
        printf("%s",menu);
        scanf("%d", &op);
        switch(op)
        {
            case 1:
                menuinge(ed, inge);
                break;
            case 2:
                menurh(ed, inge);
                break;
            case 3:
                printf("Gracias por utilizar el programa");
                break;
            default:
                printf("Opcion no valida, favor de ingresar otra opcion");
                break;
        }
    }
    free(inge);
    free(ed);
    return 0;
}

//**Menu ingenieros**
void menuinge(edificacion* ed, ingeniero* inge)
{
    int oping = 0;
    char * menuing = (char *)malloc(sizeof(char)*300);
    strcpy(menuing,"\n1. Agregar nueva edificacion\n2. Imprimir edificaciones de una fecha\n3. Salir\n\nFavor de ingresar una opción.\n");
    while(oping!=3)
    {
        printf("%s",menuing);
        scanf("%d", &oping);
        switch(oping)
        {
            case 1:
                ed=addedificacion(ed, inge);
                break;
            case 2:
                imprimeingeniero(ed);
                break;
            case 3:
                printf("\nAdios\n");
                break;
            default:
                printf("Opcion no valida, favor de ingresar otra opcion\n");
                break;
        }
    }
}

//**Menu RH**

void menurh(edificacion* ed, ingeniero* inge)
{
    int oprh=0;
    char * menurrh = (char *)malloc(sizeof(char)*100);
    strcpy(menurrh,"\n1. Agregar nuevo trabajador\n2. Imprimir reporte \n3. Salir\n\nFavor de ingresar una opción.\n");
    while(oprh!=3)
    {
        printf("%s",menurrh);
        scanf("%d", &oprh);
        switch(oprh)
        {
            case 1:
                inge=addinge(inge);
                break;
            case 2:
                imprimerh(ed, inge);
                break;
            case 3:
                printf("\nAdios\n");
                break;
            default:
                printf("Opcion no valida, favor de ingresar otra opcion\n");
                break;
        }
    }
}

edificacion* addedificacion(edificacion* ed, ingeniero* inge)
{
    int edop=0, false =0;
    //pisos del edificio
    printf("Ingrese el numero de pisos de la edificacion\n");
    scanf("%d",&((ed+toted)->pisos));
    //periodo en el que se trabajara
    printf("Ingrese el periodo de la edificacion\n");
    scanf("%d",&((ed+toted)->periodo));
    char * menuedif = (char *)malloc(sizeof(char)*300);
    strcpy(menuedif,"\n::::\tElija el tipo de edificicacion:\t::::\n1. Edificio\n2. Torre\n3. Nave\n\nFavor de ingresar una opción.\n");
    printf("%s",menuedif);
    scanf("%d", &edop);
    switch(edop)
    {
        case 1:
            printf("Es simetrico?\n 1.-Si\n 2.-No\n");
            scanf("%d",&((ed+toted)->simetrico));
            break;
        case 2:
            printf("Diametro inferior:");
            scanf("%d",&((ed+toted)->diaminf));
            printf("Diametro superior:");
            scanf("%d",&((ed+toted)->diamsup));
            break;
        case 3:
            printf("Tipo de techo: \n 1.-Con Monitor\n 2.-A dos aguas\n 3.-Creciente\n");
            scanf("%d",&((ed+toted)->techo));
            break;
    }
    while(false==0)
    {
        printf("Ingrese su numero de nomina: ");
        scanf("%d",&((ed+toted)->inge.nomina));
        for(int i=0;i<=toting;++i)
        {
            if(((inge+i)->nomina)==((ed+toted)->inge.nomina))
            {
                printf("\ncorrecto  \n");
                false=1;
            }
        }
    }
    toted+=1;
    return ed;
}

ingeniero* addinge(ingeniero* inge)
{
    printf("Ingrese la nomina del empleado ");
    scanf("%d",&((inge+toting)->nomina));
    /*printf("Ingrese el nombre (solo su nombre). ");
     scanf("%s",(inge+toting)->nombre);
     printf("Ingrese el apellido paterno ");
     scanf("%s",(inge+toting)->apellidoP);
     printf("Ingrese el apellido materno ");
     scanf("%s",(inge+toting)->apellidoM);
     printf("Ingrese la fecha de nacimiento [dd/mm/aaaa] ");
     scanf("%s",(inge+toting)->fnac);
     printf("Ingrese la fecha de ingreso [dd/mm/aaaa] ");
     scanf("%s",(inge+toting)->fing);
     printf("Ingrese el salario ");
     scanf("%d",&((inge+toting)->salario));
     printf("Ingrese el rol ");
     scanf("%s",(inge+toting)->puesto);*/
    toting+=1;
    return inge;
}

void imprimeingeniero(edificacion* ed)
{
    edificacion *temp = ed;
    int repfecha, repnomina;
    printf("\nIngrese su numero de nomina: \n");
    scanf("%d",&repnomina);
    for(int i=0;i<=toting;++i)
    {
        if(repnomina==((temp+i)->inge.nomina))
        {
            printf("\nIngrese la fecha de la que desea obtener los reportes: \n");
            scanf("%d",&repfecha);
            for(int j=0;j<=toted;++j)
            {
                if(repfecha==((temp+j)->periodo))
                {
                    printf("Cantidad de pisos: %d\n",(temp+j)->pisos);
                    printf("El edificio es simetrico (0- no es edificio, 1 si, 2 no: %d\n",(temp+j)->simetrico);
                    printf("El diametro inferior es (0 si no es una torre): %d\n",(temp+j)->diaminf);
                    printf("El diametro superior es (0 si no es una torre): %d\n",(temp+j)->diamsup);
                    printf("El tipo de techo es (0 no es nave 1.-Con Monitor 2.-A dos aguas 3.-Creciente): %d\n",(temp+j)->techo);
                }
            }
        }
    }
    
    
}

void imprimerh(edificacion* ed, ingeniero* inge)
{
    edificacion *temped = ed;
    ingeniero *temping =inge;
    int repfecha, repnomina;
    printf("\nIngrese su numero de nomina: \n");
    scanf("%d",&repnomina);
    for(int i=0;i<=toting;++i)
    {
        if(repnomina==((temped+i)->inge.nomina))
        {
            printf("\nIngrese la fecha de la que desea obtener los reportes: \n");
            scanf("%d",&repfecha);
            for(int j=0;j<=toted;++j)
            {
                if(repfecha==((temped+j)->periodo))
                {
                    printf("Cantidad de pisos: %d\n",(temped+j)->pisos);
                    printf("El edificio es simetrico (0- no es edificio, 1 si, 2 no: %d\n",(temped+j)->simetrico);
                    printf("El diametro inferior es (0 si no es una torre): %d\n",(temped+j)->diaminf);
                    printf("El diametro superior es (0 si no es una torre): %d\n",(temped+j)->diamsup);
                    printf("El tipo de techo es (0 no es nave 1.-Con Monitor 2.-A dos aguas 3.-Creciente): %d\n",(temped+j)->techo);
                    printf("Realizado por:\n");
                    for(int x=0;x<toting;++x)
                    {
                        if((temped+x)->inge.nombre==(temping+i)->nombre)
                            printf("%d.- %s %s %s",x,(temping+x)->nombre,(temping+x)->apellidoP,(temping+x)->apellidoM);
                    }
                }
            }
        }
    }
}
